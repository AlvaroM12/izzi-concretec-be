package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.EmployeeEquipo;
import com.izzisoft.proyectodh.model.ModelBase;

public class EmployeeEquipoCommand extends ModelBase {

    private Long employeeId;
    private Long equipoId;
    private String description;

    private String equipoName;
    private String equipoDescription;

    public EmployeeEquipoCommand() {
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getEquipoId() {
        return equipoId;
    }

    public void setEquipoId(Long equipoId) {
        this.equipoId = equipoId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEquipoName() {
        return equipoName;
    }

    public void setEquipoName(String equipoName) {
        this.equipoName = equipoName;
    }

    public String getEquipoDescription() {
        return equipoDescription;
    }

    public void setEquipoDescription(String equipoDescription) {
        this.equipoDescription = equipoDescription;
    }

    public EmployeeEquipoCommand(EmployeeEquipo employeeEquipo) {
        setId(employeeEquipo.getId());
        if (null != employeeEquipo.getEmployee())
            setEmployeeId(employeeEquipo.getEmployee().getId());
        if (null != employeeEquipo.getEquipo()) {
            setEquipoId(employeeEquipo.getEquipo().getId());
            setEquipoName(employeeEquipo.getEquipo().getCode());
            setEquipoDescription(employeeEquipo.getEquipo().getDescripcion());
        }
        setDescription(employeeEquipo.getDescription());
    }

    public EmployeeEquipo toEntity() {
        EmployeeEquipo employeeEquipo = new EmployeeEquipo();
        employeeEquipo.setDescription(getDescription());
        employeeEquipo.setId(getId());
        return employeeEquipo;
    }
}
