package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Equipo;
import com.izzisoft.proyectodh.model.ModelBase;

public class EquipoCommand extends ModelBase {

    private String name;
    private String code;
    private String description;
    private Long tipoEquipoId;
    private String tipoEquipoName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTipoEquipoId() {
        return tipoEquipoId;
    }

    public void setTipoEquipoId(Long tipoEquipoId) {
        this.tipoEquipoId = tipoEquipoId;
    }

    public String getTipoEquipoName() {
        return tipoEquipoName;
    }

    public void setTipoEquipoName(String tipoEquipoName) {
        this.tipoEquipoName = tipoEquipoName;
    }

    public EquipoCommand() {
    }

    public EquipoCommand (Equipo equipo) {
        if (null != equipo.getTipoEquipo()) {
            setTipoEquipoId(equipo.getTipoEquipo().getId());
            setTipoEquipoName(equipo.getTipoEquipo().getNombre_equipo());
        }
        setId(equipo.getId());
        setCode(equipo.getCode());
        setName(equipo.getName());
        setDescription(equipo.getDescripcion());
    }

    public Equipo toEntity() {
        Equipo equipo = new Equipo();
        equipo.setId(getId());
        equipo.setCode(getCode());
        equipo.setName(getName());
        equipo.setDescripcion(getDescription());
        return equipo;
    }
}
