/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.TipoEquipo;

public class TipoEquipoCommand extends ModelBase {

    private String code;
    private String nombre_equipo;
    private String nombre_maquina;
    private Long id;

    public TipoEquipoCommand(TipoEquipo tipoEquipo) {
        setId(tipoEquipo.getId());
        setCreatedOn(tipoEquipo.getCreatedOn());
        setUpdatedOn(tipoEquipo.getUpdatedOn());
        this.setCode(tipoEquipo.getCode());
        this.setNombre_equipo(tipoEquipo.getNombre_equipo());
        this.setNombre_maquina(tipoEquipo.getNombre_maquina());
    }

    public TipoEquipoCommand() {

    }

    public String getCode() {
        return code;
    }

    public String getNombre_equipo() {
        return nombre_equipo;
    }

    public String getNombre_maquina() {
        return nombre_maquina;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setNombre_equipo(String nombre_equipo) {
        this.nombre_equipo = nombre_equipo;
    }

    public void setNombre_maquina(String nombre_maquina) {
        this.nombre_maquina = nombre_maquina;
    }

    public TipoEquipo toTipoEquipo() {
        TipoEquipo tipoEquipo = new TipoEquipo();
        tipoEquipo.setId(getId());
        tipoEquipo.setCode(this.getCode());
        tipoEquipo.setNombre_equipo(this.getNombre_equipo());
        tipoEquipo.setNombre_maquina(this.getNombre_maquina());
        return tipoEquipo;
    }
}