package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.ObraEtapa;

public class ObraEtapaCommand extends ModelBase {

    private Long etapaId;

    private Long obraId;

    private String description;

    private String status;

    private String etapaName;

    public ObraEtapaCommand() {
    }

    public Long getEtapaId() {
        return etapaId;
    }

    public void setEtapaId(Long etapaId) {
        this.etapaId = etapaId;
    }

    public Long getObraId() {
        return obraId;
    }

    public void setObraId(Long obraId) {
        this.obraId = obraId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEtapaName() {
        return etapaName;
    }

    public void setEtapaName(String etapaName) {
        this.etapaName = etapaName;
    }

    public ObraEtapa toEntity() {
        ObraEtapa obraEtapa = new ObraEtapa();
        obraEtapa.setId(getId());
        obraEtapa.setStatus(getStatus());
        obraEtapa.setDescription(getDescription());
        return obraEtapa;
    }

    public ObraEtapaCommand(ObraEtapa obraEtapa) {
        setId(obraEtapa.getId());
        setStatus(obraEtapa.getStatus());
        setDescription(obraEtapa.getDescription());
        if (null != obraEtapa.getEtapa()) {
            setEtapaId(obraEtapa.getEtapa().getId());
            setEtapaName(obraEtapa.getEtapa().getNombre());
        }
        if (null != obraEtapa.getObra()) {
            setObraId(obraEtapa.getObra().getId());
        }
    }
}
