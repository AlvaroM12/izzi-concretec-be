package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.TypeAccident;

public class TypeAccidentCommand extends ModelBase {

    private String name;
    private String description;

    public TypeAccidentCommand() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TypeAccidentCommand(TypeAccident typeAccident) {
        setId(typeAccident.getId());
        setName(typeAccident.getName());
        setDescription(typeAccident.getDescription());
    }

    public TypeAccident toEntity() {
        TypeAccident typeAccident = new TypeAccident();
        typeAccident.setId(getId());
        typeAccident.setName(getName());
        typeAccident.setDescription(getDescription());
        return typeAccident;
    }
}
