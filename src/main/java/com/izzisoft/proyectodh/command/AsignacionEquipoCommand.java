/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.AsignacionEquipo;

import java.util.Date;

public class AsignacionEquipoCommand extends ModelBase {

    private String encargado;
    private Date fecha_asignacion;
    private Date fecha_devolucion;
    private String observacionInicial;
    private String observacionFinal;
    private Long id;

    public AsignacionEquipoCommand(AsignacionEquipo asignacionEquipo) {
        setId(asignacionEquipo.getId());
        setCreatedOn(asignacionEquipo.getCreatedOn());
        setUpdatedOn(asignacionEquipo.getUpdatedOn());
        this.setFecha_asignacion(asignacionEquipo.getFecha_asignacion());
        this.setEncargado(asignacionEquipo.getEncargado());
        this.setFecha_devolucion(asignacionEquipo.getFecha_devolucion());
        this.setObservacionInicial(asignacionEquipo.getObservacion_inicial());
        this.setObservacionFinal(asignacionEquipo.getObservacion_final());
    }

    public AsignacionEquipoCommand() {

    }

    public Date getFecha_asignacion() {
        return fecha_asignacion;
    }

    public String getEncargado() {
        return encargado;
    }

    public Date getFecha_devolucion() {
        return fecha_devolucion;
    }

    public String getObservacionInicial() {
        return observacionInicial;
    }

    public String getObservacionFinal() {
        return observacionFinal;
    }


    public Long getId() {
        return id;
    }

    public void setFecha_asignacion(Date fecha_asignacion) {
        this.fecha_asignacion = fecha_asignacion;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public void setFecha_devolucion(Date fecha_devolucion) {
        this.fecha_devolucion = fecha_devolucion;
    }

    public void setObservacionInicial(String observacionInicial) {
        this.observacionInicial = observacionInicial;
    }

    public void setObservacionFinal(String observacionFinal) {
        this.observacionFinal = observacionFinal;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AsignacionEquipo toAsignacionEquipo() {
        AsignacionEquipo asignacionEquipo = new AsignacionEquipo();
        asignacionEquipo.setId(getId());
        asignacionEquipo.setFecha_asignacion(this.getFecha_asignacion());
        asignacionEquipo.setEncargado(this.getEncargado());
        asignacionEquipo.setFecha_devolucion(this.getFecha_devolucion());
        asignacionEquipo.setObservacion_inicial(this.getObservacionInicial());
        asignacionEquipo.setObservacion_final(this.getObservacionFinal());
        return asignacionEquipo;
    }
}