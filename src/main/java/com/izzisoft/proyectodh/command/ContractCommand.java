package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Contract;
import com.izzisoft.proyectodh.model.ModelBase;

import java.util.Date;

public class ContractCommand extends ModelBase {
    private Long employeeId;

    private Long tipoContractId;

    private Date initDate;
    private Date endDate;

    private String description;

    public ContractCommand() {
    }

    public ContractCommand (Contract contract) {
        setId(contract.getId());
        setInitDate(contract.getInitDate());
        setEndDate(contract.getEndDate());
        if (null != contract.getEmployee())
            setEmployeeId(contract.getEmployee().getId());
        if (null != contract.getTipoContract())
            setTipoContractId(contract.getTipoContract().getId());
        setDescription(contract.getDescription());
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getTipoContractId() {
        return tipoContractId;
    }

    public void setTipoContractId(Long tipoContractId) {
        this.tipoContractId = tipoContractId;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Contract toContract() {
        Contract contract = new Contract();
        contract.setId(getId());
        contract.setInitDate(getInitDate());
        contract.setEndDate(getEndDate());
        contract.setDescription(getDescription());
        return contract;
    }
}
