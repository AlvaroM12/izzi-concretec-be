package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.AsignacionEmpleado;
import com.izzisoft.proyectodh.model.ModelBase;

import java.util.Date;

public class AsignacionEmpleadoCommand extends ModelBase {
    private String code_emp;
    private Date fecha_asignacion;
    private Date fecha_conclucion;
    private Date fecha_retiro;
    private Long id;

    public AsignacionEmpleadoCommand(AsignacionEmpleado asignacionEmpleado) {
        setId(asignacionEmpleado.getId());
        setCreatedOn(asignacionEmpleado.getCreatedOn());
        setUpdatedOn(asignacionEmpleado.getUpdatedOn());
        this.setCode_emp(asignacionEmpleado.getCodeEmp());
        this.setFecha_asignacion(asignacionEmpleado.getFechaAsignacion());
        this.setFecha_retiro(asignacionEmpleado.getFechaRetiro());
        this.setFecha_conclucion(asignacionEmpleado.getFechaConclusion());

    }

    public AsignacionEmpleadoCommand() {

    }

    public String getCode_emp() {
        return code_emp;
    }

    public void setCode_emp(String code_emp) {
        this.code_emp = code_emp;
    }

    public Date getFecha_asignacion() {
        return fecha_asignacion;
    }

    public void setFecha_asignacion(Date fecha_asignacion) {
        this.fecha_asignacion = fecha_asignacion;
    }

    public Date getFecha_conclucion() {
        return fecha_conclucion;
    }

    public void setFecha_conclucion(Date fecha_conclucion) {
        this.fecha_conclucion = fecha_conclucion;
    }

    public Date getFecha_retiro() {
        return fecha_retiro;
    }

    public void setFecha_retiro(Date fecha_retiro) {
        this.fecha_retiro = fecha_retiro;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public AsignacionEmpleado toAsignacionEmpleado() {
        AsignacionEmpleado asignacionEmpleado = new AsignacionEmpleado();
        asignacionEmpleado.setId(getId());
        asignacionEmpleado.setCodeEmp(this.getCode_emp());
        asignacionEmpleado.setFechaAsignacion(this.getFecha_asignacion());
        asignacionEmpleado.setFechaConclusion(this.getFecha_conclucion());
        asignacionEmpleado.setFechaRetiro(this.getFecha_retiro());
        return asignacionEmpleado;
    }
}
