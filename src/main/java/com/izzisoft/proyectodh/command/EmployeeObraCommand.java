package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.EmployeeObra;
import com.izzisoft.proyectodh.model.ModelBase;

public class EmployeeObraCommand extends ModelBase {

    private Long employeeId;
    private Long obraId;
    private String nameObra;
    private String description;

    public EmployeeObraCommand() {
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getObraId() {
        return obraId;
    }

    public void setObraId(Long obraId) {
        this.obraId = obraId;
    }

    public String getNameObra() {
        return nameObra;
    }

    public void setNameObra(String nameObra) {
        this.nameObra = nameObra;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EmployeeObra toEntity() {
        EmployeeObra employeeObra = new EmployeeObra();
        employeeObra.setId(getId());
        return employeeObra;
    }

    public EmployeeObraCommand(EmployeeObra employeeObra) {
        setId(employeeObra.getId());
        if (null != employeeObra.getEmployee())
            setEmployeeId(employeeObra.getEmployee().getId());
        if (null != employeeObra.getObra()) {
            setObraId(employeeObra.getObra().getId());
            setNameObra(employeeObra.getObra().getNombre());
            setDescription(employeeObra.getObra().getDescripcion());
        }
    }
}
