/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.Position;

public class PositionCommand extends ModelBase {

    private String name;
    private String code;
    private String descripcion;
    private Long id;

    public PositionCommand(Position position) {
        setId(position.getId());
        setCreatedOn(position.getCreatedOn());
        setUpdatedOn(position.getUpdatedOn());
        this.setCode(position.getCode());
        this.setDescripcion(position.getDescripcion());
        this.setName(position.getName());
    }

    public PositionCommand() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Position toPosition() {
        Position position = new Position();
        position.setId(getId());
        position.setCode(this.getCode());
        position.setDescripcion(this.getDescripcion());
        position.setName(this.getName());
        return position;
    }
}
