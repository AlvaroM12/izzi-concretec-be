/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Contract;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.model.ModelBase;
import org.apache.tomcat.util.codec.binary.Base64;

import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeCommand extends ModelBase {

    private String firstName;
    private String lastName;
    private String name;
    private Boolean featured;
    private String image;
    private int ci;
    private int telefono;
    private int celular;
    private Date fechaNacimiento;
    private String estadoCivil;
    private String direccion;
    private String segundaDireccion;

    private String jobPosition;
    private String jobCode;
    private String jobDescription;
    private Long departamentId;
    private Long cargoId;

    public Long getCargoId() {
        return cargoId;
    }

    public void setCargoId(Long cargoId) {
        this.cargoId = cargoId;
    }

    public Long getDepartamentId() {
        return departamentId;
    }

    public void setDepartamentId(Long departamentId) {
        this.departamentId = departamentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EmployeeCommand() {
    }

    public EmployeeCommand(Employee employee) {
        setId(employee.getId());
        setCreatedOn(employee.getCreatedOn());
        setUpdatedOn(employee.getUpdatedOn());
        if (null != employee.getDepartamento()) {
            setDepartamentId(employee.getDepartamento().getId());
        }
        if (null != employee.getCargo())
            setCargoId(employee.getCargo().getId());
        setFirstName(employee.getFirstName());
        setLastName(employee.getLastName());
        for (Contract contract : employee.getContracts()) {
            if (null != contract.getPosition()) {
                jobPosition = contract.getPosition().getName();
                jobCode = contract.getPosition().getCode();
                jobDescription = contract.getPosition().getDescripcion();
            }
        }
        featured = true;
        setImageBase64(employee);
        setCi(employee.getCi());
        setCelular(employee.getCelular());
        setTelefono(employee.getTelefono());
        if(employee.getFechaNacimiento()!=null)
            setFechaNacimiento(getFechaNacimiento());
        else
            setFechaNacimiento(new Date());
        setEstadoCivil(employee.getEstadoCivil());
        setDireccion(employee.getDireccion());
        setSegundaDireccion(employee.getSegundaDireccion());
    }

    private void setImageBase64(Employee employee) {
        if (employee.getImage() != null) {
            byte[] bytes = new byte[employee.getImage().length];
            for (int i = 0; i < employee.getImage().length; i++) {
                bytes[i] = employee.getImage()[i];
            }
            String imageStr = Base64.encodeBase64String(bytes);
            this.setImage(imageStr);

        }
    }

    public String getName() {
        return getFirstName() + " " + getLastName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSegundaDireccion() {
        return segundaDireccion;
    }

    public void setSegundaDireccion(String segundaDireccion) {
        this.segundaDireccion = segundaDireccion;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public Employee toEmployee() {
        Employee employee = new Employee();
        employee.setFirstName(getFirstName());
        employee.setEstadoCivil(getEstadoCivil());
        employee.setLastName(getLastName());
        employee.setTelefono(getTelefono());
        employee.setFechaNacimiento(getFechaNacimiento());
        employee.setCelular(getCelular());
        employee.setCi(getCi());
        employee.setDireccion(getDireccion());
        employee.setId(getId());
        employee.setCreatedOn(getCreatedOn());
        employee.setUpdatedOn(getUpdatedOn());
        return employee;
    }
}
