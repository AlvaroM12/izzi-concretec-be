/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Departamento;
import com.izzisoft.proyectodh.model.ModelBase;

public class DepartamentoCommand extends ModelBase {

    private String depName;
    private String depCode;
    private Long id;

    public DepartamentoCommand(Departamento departamento) {
        setId(departamento.getId());
        setCreatedOn(departamento.getCreatedOn());
        setUpdatedOn(departamento.getUpdatedOn());
        this.setDepCode(departamento.getDepCode());
        this.setDepName(departamento.getDepName());
    }

    public DepartamentoCommand() {

    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Departamento toDepartamento() {
        Departamento departamento = new Departamento();
        departamento.setId(getId());
        departamento.setDepCode(getDepCode());
        departamento.setDepName(getDepName());
        return departamento;
    }
}
