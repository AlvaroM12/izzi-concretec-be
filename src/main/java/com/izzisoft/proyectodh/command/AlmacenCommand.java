/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Almacen;
import com.izzisoft.proyectodh.model.ModelBase;

public class AlmacenCommand extends ModelBase {

    private String name;
    private String code;
    private String ubicacion;
    private Long id;

    public AlmacenCommand(Almacen almacen) {
        setId(almacen.getId());
        setCreatedOn(almacen.getCreatedOn());
        setUpdatedOn(almacen.getUpdatedOn());
        this.setCode(almacen.getCode());
        this.setUbicacion(almacen.getUbicacion());
        this.setName(almacen.getName());
    }

    public AlmacenCommand() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Almacen toAlmacen() {
        Almacen almacen = new Almacen();
        almacen.setId(getId());
        almacen.setCode(this.getCode());
        almacen.setUbicacion(this.getUbicacion());
        almacen.setName(this.getName());
        return almacen;
    }
}
