package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Etapa;
import com.izzisoft.proyectodh.model.ModelBase;

import java.util.Date;

public class EtapaCommand extends ModelBase {
    private String code;
    private String nombre;
    private Date fechaInicio;
    private Date fechaFin;
    private int nroEmpleados;

    public EtapaCommand(Etapa etapa) {
        setId(etapa.getId());
        setCreatedOn(etapa.getCreatedOn());
        setUpdatedOn(etapa.getUpdatedOn());
        this.setCode(etapa.getCode());
        this.setNombre(etapa.getNombre());
        this.setFechaInicio(etapa.getFechaInicio());
        this.setFechaFin(etapa.getFechaFin());
        this.setNroEmpleados(etapa.getNroEmpleados());

    }

    public EtapaCommand() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getNroEmpleados() {
        return nroEmpleados;
    }

    public void setNroEmpleados(int nroEmpleados) {
        this.nroEmpleados = nroEmpleados;
    }

    public Etapa toEtapa() {
        Etapa etapa = new Etapa();
        etapa.setId(getId());
        etapa.setCode(this.getCode());
        etapa.setNombre(this.getNombre());
        etapa.setFechaInicio(this.getFechaInicio());
        etapa.setFechaFin(this.getFechaFin());
        etapa.setNroEmpleados(this.getNroEmpleados());
        return etapa;
    }
}
