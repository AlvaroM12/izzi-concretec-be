package com.izzisoft.proyectodh.command;


import com.izzisoft.proyectodh.model.Area;
import com.izzisoft.proyectodh.model.ModelBase;


public class AreaCommand extends ModelBase {
    private String code;
    private String nombre;
    //Entidad Responsable
    private String responsable;
    private Long id;

    public AreaCommand(Area area) {
        setId(area.getId());
        setCreatedOn(area.getCreatedOn());
        setUpdatedOn(area.getUpdatedOn());
        this.setCode(area.getCode());
        this.setNombre(area.getNombre());
        //this.setResponsable(area.getResponsable());

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Area toArea() {
        Area area = new Area();
        area.setId(getId());
        area.setCode(this.getCode());
        area.setNombre(this.getNombre());
        //etapa.setEncargado(this.getEncargado());
        return area;
    }
}
