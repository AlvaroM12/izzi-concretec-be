package com.izzisoft.proyectodh.command;

public class KvalueCommand {
    private String k;
    private String v;

    public KvalueCommand(String k, String v) {
        this.setK(k);
        this.setV(v);
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }
}
