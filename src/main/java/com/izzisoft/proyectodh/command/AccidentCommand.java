package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.Accident;
import com.izzisoft.proyectodh.model.ModelBase;

public class AccidentCommand extends ModelBase {

    private Long employeeId;

    private Long typeAccidentId;

    private String description;

    private String employeeData;

    private String typeAccidentData;

    public AccidentCommand() {
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getTypeAccidentId() {
        return typeAccidentId;
    }

    public void setTypeAccidentId(Long typeAccidentId) {
        this.typeAccidentId = typeAccidentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmployeeData() {
        return employeeData;
    }

    public void setEmployeeData(String employeeData) {
        this.employeeData = employeeData;
    }

    public String getTypeAccidentData() {
        return typeAccidentData;
    }

    public void setTypeAccidentData(String typeAccidentData) {
        this.typeAccidentData = typeAccidentData;
    }

    public AccidentCommand(Accident accident) {
        setId(accident.getId());
        setDescription(accident.getDescription());
        if (null != accident.getEmployee()) {
            setEmployeeId(accident.getEmployee().getId());
            setEmployeeData(accident.getEmployee().getFirstName() +" "+ accident.getEmployee().getFirstName());
        }
        if (null != accident.getTypeAccident()) {
            setTypeAccidentId(accident.getTypeAccident().getId());
            setTypeAccidentData(accident.getTypeAccident().getName());
        }
    }

    public Accident toEntity() {
        Accident accident = new Accident();
        accident.setId(getId());
        accident.setDescription(getDescription());
        return accident;
    }
}
