package com.izzisoft.proyectodh.command;

import com.izzisoft.proyectodh.model.ModelBase;
import com.izzisoft.proyectodh.model.Obra;

import java.util.Date;

public class ObraCommand extends ModelBase {

    private String code;
    private String nombre;
    private String descripcion;
    private Date fecha_inicio;
    private Date fecha_fin;
    private String ubicacion;
    private Long id;

    public ObraCommand(Obra obra) {
        setId(obra.getId());
        setCreatedOn(obra.getCreatedOn());
        setUpdatedOn(obra.getUpdatedOn());
        this.setCode(obra.getCode());
        this.setDescripcion(obra.getDescripcion());
        this.setNombre(obra.getNombre());
        this.setFecha_inicio(obra.getFechaInicio());
        this.setFecha_fin(obra.getFechaFin());
        this.setUbicacion(obra.getUbicacion());
    }

    public ObraCommand() {

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }


    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Obra toObra() {
        Obra obra = new Obra();
        obra.setId(getId());
        obra.setCode(this.getCode());
        obra.setDescripcion(this.getDescripcion());
        obra.setNombre(this.getNombre());
        obra.setFechaInicio(this.getFecha_inicio());
        obra.setFechaFin(this.getFecha_fin());
        obra.setUbicacion(this.getUbicacion());
        return obra;
    }
}
