package com.izzisoft.proyectodh.controller;


import com.izzisoft.proyectodh.command.EquipoCommand;
import com.izzisoft.proyectodh.model.Equipo;
import com.izzisoft.proyectodh.services.EquipoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/equipos")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class EquipoController {

    private EquipoService equipoService;

    public EquipoController(EquipoService equipoService) {
        this.equipoService = equipoService;
    }

    @GET
    public Response getEmployeeEquipos(@QueryParam("tipo") Long tipo) {
        List<EquipoCommand> employeeList = new ArrayList<>();
        equipoService.findAllEquipo(tipo).forEach(employee -> {
            employeeList.add(new EquipoCommand(employee));
        });
        return Response.ok(employeeList).build();
    }

    @GET
    @Path("/{id}")
    public Response getEmployeeEquipoById(@PathParam("id") long id) {
        Equipo employeeObra = equipoService.findById(id);
        return Response.ok(new EquipoCommand(employeeObra)).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public EquipoCommand addEmployee(EquipoCommand equipoCommand) {
        Equipo equipo = equipoService.saveEquipo(equipoCommand);
        return new EquipoCommand(equipo);
    }

    @PUT
    public EquipoCommand updateEmployee(EquipoCommand employeeObraCommand) {
        Equipo employee = equipoService.saveEquipo(employeeObraCommand);
        return new EquipoCommand(employee);
    }

    @DELETE
    @Path("/{id}")
    public void deleteEmployee(@PathParam("id") long id) {
        equipoService.deleteById(id);
    }
}
