/**
 * @author:
 */

package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.TipoEquipoCommand;
import com.izzisoft.proyectodh.model.TipoEquipo;
import com.izzisoft.proyectodh.services.TipoEquipoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/tipoEquipos")
@Produces("application/json")
@CrossOrigin
public class TipoEquipoController {
    private TipoEquipoService service;

    public TipoEquipoController(TipoEquipoService service) {
        this.service = service;
    }

    @GET
    public Response getTipoEquipos() {
        List<TipoEquipoCommand> tipoEquipos = new ArrayList<>();
        service.findAll().forEach(tipoEquipo -> {
            TipoEquipoCommand tipoEquipoCommand = new TipoEquipoCommand(tipoEquipo);
            tipoEquipos.add(tipoEquipoCommand);
        });
        return Response.ok(tipoEquipos).build();
    }

    @GET
    @Path("/{id}")
    public Response getTipoEquiposById(@PathParam("id") @NotNull Long id) {
        TipoEquipo TipoEquipo = service.findById(id);
        return Response.ok(new TipoEquipoCommand(TipoEquipo)).build();
    }

    @POST
    public Response saveTipoEquipo(TipoEquipoCommand TipoEquipo) {
        TipoEquipo model = TipoEquipo.toTipoEquipo();
        TipoEquipo TipoEquipoPersisted = service.save(model);
        return Response.ok(new TipoEquipoCommand(TipoEquipoPersisted)).build();
    }

    @PUT
    public Response updateTipoEquipo(TipoEquipoCommand tipoEquipo) {
        TipoEquipo tipoEquipoPersisted = service.save(tipoEquipo.toTipoEquipo());
        return Response.ok(new TipoEquipoCommand(tipoEquipoPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTipoEquipo(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}