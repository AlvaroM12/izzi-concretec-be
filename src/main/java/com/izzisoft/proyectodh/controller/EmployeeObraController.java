package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.EmployeeObraCommand;
import com.izzisoft.proyectodh.model.EmployeeObra;
import com.izzisoft.proyectodh.services.EmployeeObraService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/employeesobras")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class EmployeeObraController {

    private EmployeeObraService employeeObraService;

    public EmployeeObraController(EmployeeObraService employeeObraService) {
        this.employeeObraService = employeeObraService;
    }

    @GET
    public Response getEmployeeObras() {
        List<EmployeeObraCommand> employeeList = new ArrayList<>();
        employeeObraService.findAll().forEach(employee -> {
            employeeList.add(new EmployeeObraCommand(employee));
        });
        return Response.ok(employeeList).build();
    }

    @GET
    @Path("/{id}")
    public Response getEmployeeObraById(@PathParam("id") long id) {
        EmployeeObra employeeObra = employeeObraService.findById(id);
        return Response.ok(new EmployeeObraCommand(employeeObra)).build();
    }

    @GET
    @Path("employee/{id}")
    public Response getEmployeeObraByEmployee(@PathParam("id") long id) {
        List<EmployeeObraCommand> employeeObras = new ArrayList<>();
        employeeObraService.getEmployeeObras(id).forEach(employeeObra -> {
            employeeObras.add(new EmployeeObraCommand(employeeObra));
        });
        return Response.ok(employeeObras).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public EmployeeObraCommand addEmployee(EmployeeObraCommand employeeObraCommand) {
        EmployeeObra employeeObra = employeeObraService.saveEmployeeObra(employeeObraCommand);
        return new EmployeeObraCommand(employeeObra);
    }

    @PUT
    public EmployeeObraCommand updateEmployee(EmployeeObraCommand employeeObraCommand) {
        EmployeeObra employee = employeeObraService.save(employeeObraCommand.toEntity());
        return new EmployeeObraCommand(employee);
    }

    @DELETE
    @Path("/{id}")
    public void deleteEmployee(@PathParam("id") long id) {
        employeeObraService.deleteById(id);
    }
}
