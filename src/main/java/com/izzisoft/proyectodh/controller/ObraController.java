package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.ObraCommand;
import com.izzisoft.proyectodh.model.Obra;
import com.izzisoft.proyectodh.services.ObraService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@Path("/obras")
@Produces("application/json")
@CrossOrigin
public class ObraController {
    private ObraService service;

    public ObraController(ObraService obra) {
        this.service = obra;
    }

    @GET
    public Response getObras(@QueryParam("fechaInicio") String fechaInicio,
                             @QueryParam("fechaFin") String fechaFin) {
        List<ObraCommand> obras = new ArrayList<>();
        service.findObraAll(fechaInicio, fechaFin).forEach(obra -> {
            ObraCommand obraCommand = new ObraCommand(obra);
            obras.add(obraCommand);
        });
        return Response.ok(obras).build();
    }

    @GET
    @Path("/{id}")
    public Response getObrasById(@PathParam("id") @NotNull Long id) {
        Obra Obra = service.findById(id);
        return Response.ok(new ObraCommand(Obra)).build();
    }

    @POST
    public Response saveObra(ObraCommand Obra) {
        Obra model = Obra.toObra();
        Obra ObraPersisted = service.save(model);
        return Response.ok(new ObraCommand(ObraPersisted)).build();
    }

    @PUT
    public Response updateObra(ObraCommand Obra) {
        Obra ObraPersisted = service.save(Obra.toObra());
        return Response.ok(new ObraCommand(ObraPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteObra(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}
