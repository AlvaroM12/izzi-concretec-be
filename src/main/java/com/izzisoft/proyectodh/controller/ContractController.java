package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.ContractCommand;
import com.izzisoft.proyectodh.model.Contract;
import com.izzisoft.proyectodh.services.ContractService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/contracts")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class ContractController {

    private ContractService contractService;

    public ContractController(ContractService contractService) {
        this.contractService = contractService;
    }

    @GET
    public Response getContracts() {
        List<ContractCommand> contractCommands = new ArrayList<>();
        contractService.findAll().forEach(employee -> {
            contractCommands.add(new ContractCommand(employee));
        });
        return Response.ok(contractCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getContractById(@PathParam("id") long id) {
        Contract employee = contractService.findById(id);
        return Response.ok(new ContractCommand(employee)).build();
    }

    @GET
    @Path("employee/{id}")
    public Response getContractByEmployeeId(@PathParam("id") long id) {
        Contract employee = contractService.getContractByEmployee(id);
        return Response.ok(new ContractCommand(employee)).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public ContractCommand addContract(ContractCommand contractCommand) {
        Contract contract = contractService.saveContractEmployee(contractCommand);
        return new ContractCommand(contract);
    }

    @PUT
    public ContractCommand updateContract(ContractCommand contractCommand) {
        Contract contract = contractService.saveContractEmployee(contractCommand);
        return new ContractCommand(contract);
    }

    @DELETE
    @Path("/{id}")
    public void deleteContract(@PathParam("id") long id) {
        contractService.deleteById(id);
    }

}
