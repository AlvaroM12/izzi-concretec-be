/**
 * @author:
 */

package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.DepartamentoCommand;
import com.izzisoft.proyectodh.model.Departamento;
import com.izzisoft.proyectodh.services.DepartamentoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/departamentos")
@Produces("application/json")
@CrossOrigin
public class DepartamentoController {
    private DepartamentoService service;

    public DepartamentoController(DepartamentoService service) {
        this.service = service;
    }

    @GET
    public Response getDepartamentos() {
        List<DepartamentoCommand> departamentos = new ArrayList<>();
        service.findAll().forEach(item -> {
            DepartamentoCommand itemCommand = new DepartamentoCommand(item);
            departamentos.add(itemCommand);
        });
        return Response.ok(departamentos).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Departamento departamento = service.findById(id);
        return Response.ok(new DepartamentoCommand(departamento)).build();
    }

    @POST
    public Response saveDepartamento(DepartamentoCommand departamento) {
        Departamento model = departamento.toDepartamento();
        Departamento departamentoPersisted = service.save(model);
        return Response.ok(new DepartamentoCommand(departamentoPersisted)).build();
    }

    @PUT
    public Response updateItem(DepartamentoCommand departamento) {
        Departamento departamentoPersisted = service.save(departamento.toDepartamento());
        return Response.ok(new DepartamentoCommand(departamentoPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}