package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.AccidentCommand;
import com.izzisoft.proyectodh.model.Accident;
import com.izzisoft.proyectodh.services.AccidentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/accidents")
@Produces("application/json")
@CrossOrigin
public class AccidentController {

    private AccidentService accidentService;

    public AccidentController(AccidentService accidentService) {
        this.accidentService = accidentService;
    }

    @GET
    public Response getAccidents(@QueryParam("query") String query, @QueryParam("tipo") Long tipo) {
        List<AccidentCommand> accidentCommands = new ArrayList<>();
        accidentService.findAllAccidentes(tipo).forEach(employee -> {
            accidentCommands.add(new AccidentCommand(employee));
        });
        return Response.ok(accidentCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getAccidentById(@PathParam("id") long id) {
        Accident cargo = accidentService.findById(id);
        return Response.ok(cargo).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public Accident addAccident(AccidentCommand accidentCommand) {
        Accident save = accidentService.saveAccident(accidentCommand);
        return save;
    }

    @PUT
    public Accident updateAccident(AccidentCommand accident) {
        Accident updateEntity = accidentService.saveAccident(accident);
        return updateEntity;
    }

    @DELETE
    @Path("/{id}")
    public void deleteAccident(@PathParam("id") long id) {
        accidentService.deleteById(id);
    }
}
