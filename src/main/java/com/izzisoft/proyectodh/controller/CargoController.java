package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.model.Cargo;
import com.izzisoft.proyectodh.services.CargoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
@Path("/cargos")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class CargoController {

    private CargoService cargoService;

    public CargoController(CargoService cargoService) {
        this.cargoService = cargoService;
    }

    @GET
    public Response getCargos() {
        return Response.ok(cargoService.findAll()).build();
    }

    @GET
    @Path("/{id}")
    public Response getCargoById(@PathParam("id") long id) {
        Cargo cargo = cargoService.findById(id);
        return Response.ok(cargo).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public Cargo addCargo(Cargo cargo) {
        Cargo save = cargoService.save(cargo);
        return save;
    }

    @PUT
    public Cargo updateCargo(Cargo cargo) {
        Cargo updateEntity = cargoService.save(cargo);
        return updateEntity;
    }

    @DELETE
    @Path("/{id}")
    public void deleteCargo(@PathParam("id") long id) {
        cargoService.deleteById(id);
    }
}
