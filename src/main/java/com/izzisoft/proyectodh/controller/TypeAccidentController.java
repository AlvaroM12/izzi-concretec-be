package com.izzisoft.proyectodh.controller;


import com.izzisoft.proyectodh.model.TypeAccident;
import com.izzisoft.proyectodh.services.TypeAccidentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Controller
@Path("/typeAccidents")
@Produces("application/json")
@CrossOrigin
public class TypeAccidentController {

    private TypeAccidentService typeAccidentService;

    public TypeAccidentController(TypeAccidentService typeAccidentService) {
        this.typeAccidentService = typeAccidentService;
    }

    @GET
    public Response getTypeAccidents() {
        return Response.ok(typeAccidentService.findAll()).build();
    }

    @GET
    @Path("/{id}")
    public Response getTypeAccidentById(@PathParam("id") long id) {
        TypeAccident cargo = typeAccidentService.findById(id);
        return Response.ok(cargo).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public TypeAccident addTypeAccident(TypeAccident cargo) {
        TypeAccident save = typeAccidentService.save(cargo);
        return save;
    }

    @PUT
    public TypeAccident updateTypeAccident(TypeAccident cargo) {
        TypeAccident updateEntity = typeAccidentService.save(cargo);
        return updateEntity;
    }

    @DELETE
    @Path("/{id}")
    public void deleteTypeAccident(@PathParam("id") long id) {
        typeAccidentService.deleteById(id);
    }
}
