package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.ItemCommand;
import com.izzisoft.proyectodh.command.LoginComand;
import com.izzisoft.proyectodh.model.Item;
import com.izzisoft.proyectodh.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Controller
@Path("/login")
@Produces("application/json")
@CrossOrigin
public class LoginController {

    @Autowired
    private UserService userService;

    @POST
    public Response saveItem(LoginComand login) {
        return Response.ok(userService.login(login)).build();
    }
}
