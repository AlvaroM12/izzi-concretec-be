package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.EmployeeEquipoCommand;
import com.izzisoft.proyectodh.model.EmployeeEquipo;
import com.izzisoft.proyectodh.services.EmployeeEquipoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/employeeEquipos")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class EmployeeEquipoController {

    private EmployeeEquipoService employeeEquipoService;

    public EmployeeEquipoController(EmployeeEquipoService employeeEquipoService) {
        this.employeeEquipoService = employeeEquipoService;
    }

    @GET
    public Response getEmployeeEquipos() {
        List<EmployeeEquipoCommand> employeeList = new ArrayList<>();
        employeeEquipoService.findAll().forEach(employee -> {
            employeeList.add(new EmployeeEquipoCommand(employee));
        });
        return Response.ok(employeeList).build();
    }

    @GET
    @Path("/{id}")
    public Response getEmployeeEquipoById(@PathParam("id") long id) {
        EmployeeEquipo employeeObra = employeeEquipoService.findById(id);
        return Response.ok(new EmployeeEquipoCommand(employeeObra)).build();
    }

    @GET
    @Path("employee/{id}")
    public Response getEmployeeEquipoByEmployee(@PathParam("id") long id) {
        List<EmployeeEquipoCommand> employeeObras = new ArrayList<>();
        employeeEquipoService.findEmployeeEquipoByEmployee(id).forEach(employeeObra -> {
            employeeObras.add(new EmployeeEquipoCommand(employeeObra));
        });
        return Response.ok(employeeObras).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public EmployeeEquipoCommand addEmployee(EmployeeEquipoCommand employeeObraCommand) {
        EmployeeEquipo employeeObra = employeeEquipoService.saveEmployeeEquipo(employeeObraCommand);
        return new EmployeeEquipoCommand(employeeObra);
    }

    @PUT
    public EmployeeEquipoCommand updateEmployee(EmployeeEquipoCommand employeeObraCommand) {
        EmployeeEquipo employee = employeeEquipoService.save(employeeObraCommand.toEntity());
        return new EmployeeEquipoCommand(employee);
    }

    @DELETE
    @Path("/{id}")
    public void deleteEmployee(@PathParam("id") long id) {
        employeeEquipoService.deleteById(id);
    }
}
