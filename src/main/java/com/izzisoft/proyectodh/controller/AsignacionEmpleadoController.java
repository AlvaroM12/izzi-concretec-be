package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.AsignacionEmpleadoCommand;
import com.izzisoft.proyectodh.model.AsignacionEmpleado;
import com.izzisoft.proyectodh.services.AsignacionEmpleadoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/asignacionEmpleados")
@Produces("application/json")
@CrossOrigin
public class AsignacionEmpleadoController {
    private AsignacionEmpleadoService service;

    public AsignacionEmpleadoController(AsignacionEmpleadoService service) {
        this.service = service;
    }

    @GET
    public Response getAsignacionEmpleados() {
        List<AsignacionEmpleadoCommand> asignacionEmpleados = new ArrayList<>();
        service.findAll().forEach(asignacionEmpleado -> {
            AsignacionEmpleadoCommand asignacionEmpleadoCommand = new AsignacionEmpleadoCommand(asignacionEmpleado);
            asignacionEmpleados.add(asignacionEmpleadoCommand);
        });
        return Response.ok(asignacionEmpleados).build();
    }

    @GET
    @Path("/{id}")
    public Response getAsignacionEmpleadosById(@PathParam("id") @NotNull Long id) {
        AsignacionEmpleado AsignacionEmpleado = service.findById(id);
        return Response.ok(new AsignacionEmpleadoCommand(AsignacionEmpleado)).build();
    }

    @POST
    public Response saveAsignacionEmpleado(AsignacionEmpleadoCommand AsignacionEmpleado) {
        AsignacionEmpleado model = AsignacionEmpleado.toAsignacionEmpleado();
        AsignacionEmpleado AsignacionEmpleadoPersisted = service.save(model);
        return Response.ok(new AsignacionEmpleadoCommand(AsignacionEmpleadoPersisted)).build();
    }

    @PUT
    public Response updateAsignacionEmpleado(AsignacionEmpleadoCommand AsignacionEmpleado) {
        AsignacionEmpleado AsignacionEmpleadoPersisted = service.save(AsignacionEmpleado.toAsignacionEmpleado());
        return Response.ok(new AsignacionEmpleadoCommand(AsignacionEmpleadoPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteAsignacionEmpleado(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}
