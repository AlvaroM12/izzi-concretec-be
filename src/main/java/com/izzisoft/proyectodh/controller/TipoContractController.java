package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.model.TipoContract;
import com.izzisoft.proyectodh.services.TipoContractService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/typecontracts")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class TipoContractController {

    private TipoContractService tipoContractService;

    public TipoContractController(TipoContractService tipoContractService) {
        this.tipoContractService = tipoContractService;
    }

    @GET
    public Response getTipoContracts() {
        return Response.ok(tipoContractService.findAll()).build();
    }

    @GET
    @Path("/{id}")
    public Response getTipoContractById(@PathParam("id") long id) {
        TipoContract cargo = tipoContractService.findById(id);
        return Response.ok(cargo).build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    @POST
    public TipoContract addTipoContract(TipoContract cargo) {
        TipoContract save = tipoContractService.save(cargo);
        return save;
    }

    @PUT
    public TipoContract updateTipoContract(TipoContract cargo) {
        TipoContract updateEntity = tipoContractService.save(cargo);
        return updateEntity;
    }

    @DELETE
    @Path("/{id}")
    public void deleteTipoContract(@PathParam("id") long id) {
        tipoContractService.deleteById(id);
    }
}
