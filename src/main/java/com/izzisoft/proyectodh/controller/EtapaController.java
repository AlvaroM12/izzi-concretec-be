package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.EtapaCommand;
import com.izzisoft.proyectodh.model.Etapa;
import com.izzisoft.proyectodh.services.EtapaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/etapas")
@Produces("application/json")
@CrossOrigin
public class EtapaController {
    private EtapaService service;

    public EtapaController(EtapaService service) {
        this.service = service;
    }

    @GET
    public Response getEtapas() {
        List<EtapaCommand> etapas = new ArrayList<>();
        service.findAll().forEach(etapa -> {
            EtapaCommand etapaCommand = new EtapaCommand(etapa);
            etapas.add(etapaCommand);
        });
        return Response.ok(etapas).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Etapa Etapa = service.findById(id);
        return Response.ok(new EtapaCommand(Etapa)).build();
    }

    @POST
    public Response saveEtapa(EtapaCommand Etapa) {
        Etapa model = Etapa.toEtapa();
        Etapa EtapaPersisted = service.save(model);
        return Response.ok(new EtapaCommand(EtapaPersisted)).build();
    }

    @PUT
    public Response updateEtapa(EtapaCommand etapa) {
        Etapa EtapaPersisted = service.save(etapa.toEtapa());
        return Response.ok(new EtapaCommand(EtapaPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteEtapa(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}
