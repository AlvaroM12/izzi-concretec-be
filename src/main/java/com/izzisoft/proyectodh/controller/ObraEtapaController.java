package com.izzisoft.proyectodh.controller;


import com.izzisoft.proyectodh.command.ObraEtapaCommand;
import com.izzisoft.proyectodh.model.ObraEtapa;
import com.izzisoft.proyectodh.services.ObraEtapaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/obraEtapas")
@Produces("application/json")
@CrossOrigin
public class ObraEtapaController {

    private ObraEtapaService obraEtapaService;

    public ObraEtapaController(ObraEtapaService obraEtapaService) {
        this.obraEtapaService = obraEtapaService;
    }

    @GET
    public Response getObras() {
        List<ObraEtapaCommand> obras = new ArrayList<>();
        obraEtapaService.findAll().forEach(obra -> {
            ObraEtapaCommand obraCommand = new ObraEtapaCommand(obra);
            obras.add(obraCommand);
        });
        return Response.ok(obras).build();
    }

    @GET
    @Path("/{id}")
    public Response getObrasById(@PathParam("id") @NotNull Long id) {
        ObraEtapa obraEtapa = obraEtapaService.findById(id);
        return Response.ok(new ObraEtapaCommand(obraEtapa)).build();
    }
    @GET
    @Path("obra/{id}")
    public Response getEtapasByObra(@PathParam("id") @NotNull Long id) {
        List<ObraEtapaCommand> obraEtapaCommands = new ArrayList<>();
        obraEtapaService.getObraEtapasByObra(id).forEach(obra -> {
            ObraEtapaCommand obraCommand = new ObraEtapaCommand(obra);
            obraEtapaCommands.add(obraCommand);
        });
        return Response.ok(obraEtapaCommands).build();
    }

    @POST
    public Response saveObra(ObraEtapaCommand obraEtapaCommand) {
        ObraEtapa obraEtapa = obraEtapaService.saveObraEtapa(obraEtapaCommand);
        return Response.ok(new ObraEtapaCommand(obraEtapa)).build();
    }

    @PUT
    public Response updateObra(ObraEtapaCommand obraEtapaCommand) {
        ObraEtapa obraEtapa = obraEtapaService.saveObraEtapa(obraEtapaCommand);
        return Response.ok(new ObraEtapaCommand(obraEtapa)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteObra(@PathParam("id") String id) {
        obraEtapaService.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
}
