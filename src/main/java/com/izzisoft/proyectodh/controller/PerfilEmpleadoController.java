/**
 * @author:
 */

package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.PerfilEmpleadoCommand;
import com.izzisoft.proyectodh.model.PerfilEmpleado;
import com.izzisoft.proyectodh.services.PerfilEmpleadoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/perfilEmpleados")
@Produces("application/json")
@CrossOrigin
public class PerfilEmpleadoController {
    private PerfilEmpleadoService service;

    public PerfilEmpleadoController(PerfilEmpleadoService service) {
        this.service = service;
    }

    @GET
    public Response getPerfilEmpleados() {
        List<PerfilEmpleadoCommand> perfilEmpleados = new ArrayList<>();
        service.findAll().forEach(perfilEmpleado -> {
            PerfilEmpleadoCommand positionCommand = new PerfilEmpleadoCommand(perfilEmpleado);
            perfilEmpleados.add(positionCommand);
        });
        return Response.ok(perfilEmpleados).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        PerfilEmpleado PerfilEmpleado = service.findById(id);
        return Response.ok(new PerfilEmpleadoCommand(PerfilEmpleado)).build();
    }

    @GET
    @Path("employee/{id}")
    public Response getProfileByEmployeeId(@PathParam("id") @NotNull Long id) {
        PerfilEmpleado PerfilEmpleado = service.findProfileByEmployeeId(id);
        return Response.ok(new PerfilEmpleadoCommand(PerfilEmpleado)).build();
    }

    @POST
    public Response savePosition(PerfilEmpleadoCommand pe) {
        PerfilEmpleado PerfilEmpleadoPersisted = service.saveProfileEmployee(pe);
        return Response.ok(new PerfilEmpleadoCommand(PerfilEmpleadoPersisted)).build();
    }

    @PUT
    public Response updatePerfilEmpleado(PerfilEmpleadoCommand perfilEmpleado) {
        PerfilEmpleado perfilEmpleadoPersisted = service.saveProfileEmployee(perfilEmpleado);
        return Response.ok(new PerfilEmpleadoCommand(perfilEmpleadoPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deletePerfilEmpleado(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}