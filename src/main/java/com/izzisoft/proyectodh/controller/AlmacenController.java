/**
 * @author:
 */

package com.izzisoft.proyectodh.controller;

import com.izzisoft.proyectodh.command.KvalueCommand;
import com.izzisoft.proyectodh.command.AlmacenCommand;
import com.izzisoft.proyectodh.model.Almacen;
import com.izzisoft.proyectodh.services.AlmacenService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/almacenes")
@Produces("application/json")
@CrossOrigin
public class AlmacenController {
    private AlmacenService service;

    public AlmacenController(AlmacenService service) {
        this.service = service;
    }

    @GET
    public Response getAlmacens(@Context UriInfo uriInfo) {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        if(queryParams.isEmpty()){
            List<AlmacenCommand> almacens = new ArrayList<>();
            service.findAll().forEach(almacen -> {
                AlmacenCommand almacenCommand = new AlmacenCommand(almacen);
                almacens.add(almacenCommand);
            });
            return Response.ok(almacens).build();
        } else {
            if(queryParams.containsKey("kv")){
                List<KvalueCommand> vals = new ArrayList<>();
                 service.findAll().forEach(almacen -> {
                    KvalueCommand kvCommand = new KvalueCommand(almacen.getId().toString(),almacen.getCode()+"-"+almacen.getName());
                    vals.add(kvCommand);
                });
                    return Response.ok(vals).build();
            } else {
                return Response.ok().build();
            }
        }
    }

    @GET
    @Path("/{id}")
    public Response getAlmacenById(@PathParam("id") @NotNull Long id) {
        Almacen Almacen = service.findById(id);
        return Response.ok(new AlmacenCommand(Almacen)).build();
    }

    @POST
    public Response saveAlmacen(AlmacenCommand Almacen) {
        Almacen model = Almacen.toAlmacen();
        Almacen AlmacenPersisted = service.save(model);
        return Response.ok(new AlmacenCommand(AlmacenPersisted)).build();
    }

    @PUT
    public Response updateAlmacen(AlmacenCommand Almacen) {
        Almacen AlmacenPersisted = service.save(Almacen.toAlmacen());
        return Response.ok(new AlmacenCommand(AlmacenPersisted)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteItem(@PathParam("id") String id) {
        service.deleteById(Long.valueOf(id));
        return Response.ok().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

}