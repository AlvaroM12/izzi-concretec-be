package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.EmployeeObra;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeObraRepository extends CrudRepository<EmployeeObra, Long> {

    List<EmployeeObra> findByEmployeeId(Long id);
}
