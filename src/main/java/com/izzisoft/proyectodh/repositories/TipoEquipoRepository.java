package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.TipoEquipo;
import org.springframework.data.repository.CrudRepository;

public interface TipoEquipoRepository extends CrudRepository<TipoEquipo, Long> {
}