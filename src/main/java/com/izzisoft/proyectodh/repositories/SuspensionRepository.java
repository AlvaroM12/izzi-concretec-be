package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Suspension;
import org.springframework.data.repository.CrudRepository;

public interface SuspensionRepository extends CrudRepository<Suspension, Long> {
}
