/**
 * @author: edson
 */

package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Employee;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Iterable<Employee> findByDepartamentoId(Long id);
}
