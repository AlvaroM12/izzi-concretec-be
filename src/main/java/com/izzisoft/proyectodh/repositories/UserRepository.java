package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmailAndPassword(String email, String password);
}
