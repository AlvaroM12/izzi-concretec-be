package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Departamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface DepartamentoRepository extends CrudRepository<Departamento, Long> {
    Optional<List<Departamento>> findByDepName(String nombre);

    @Override
    List<Departamento> findAll();
}