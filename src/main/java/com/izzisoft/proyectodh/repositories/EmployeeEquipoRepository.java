package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.EmployeeEquipo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeEquipoRepository extends CrudRepository<EmployeeEquipo, Long> {

    List<EmployeeEquipo> findByEmployeeId(Long id);
}
