package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Cargo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CargoRepository extends CrudRepository<Cargo, Long> {
}
