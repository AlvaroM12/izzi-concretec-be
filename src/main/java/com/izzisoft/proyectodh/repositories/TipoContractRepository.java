package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.TipoContract;
import org.springframework.data.repository.CrudRepository;

public interface TipoContractRepository extends CrudRepository<TipoContract, Long> {
}
