package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Equipo;
import org.springframework.data.repository.CrudRepository;

public interface EquipoRepository extends CrudRepository<Equipo, Long> {

    Iterable<Equipo> findAllByTipoEquipoId(Long id);
}
