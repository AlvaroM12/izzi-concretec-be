package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Obra;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface ObraRepository extends CrudRepository<Obra, Long> {

    Iterable<Obra> findAllByFechaInicioGreaterThanEqualAndFechaFinLessThanEqual(Date fechaInicio, Date fechaFin);
}
