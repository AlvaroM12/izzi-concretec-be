package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Area;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AreaRepository extends CrudRepository<Area, Long>{
    Optional<List<Area>> findByNombre(String nombre);

    List<Area> findAll();
}
