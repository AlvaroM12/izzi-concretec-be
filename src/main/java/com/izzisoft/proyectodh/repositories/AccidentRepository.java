package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Accident;
import org.springframework.data.repository.CrudRepository;

public interface AccidentRepository extends CrudRepository<Accident, Long> {

    Iterable<Accident> findAllByTypeAccidentId(Long id);


}
