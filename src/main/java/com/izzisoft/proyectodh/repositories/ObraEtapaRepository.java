package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.ObraEtapa;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ObraEtapaRepository extends CrudRepository<ObraEtapa, Long> {

    List<ObraEtapa> findByObraId(Long id);
}
