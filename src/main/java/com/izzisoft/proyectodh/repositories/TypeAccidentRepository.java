package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.TypeAccident;
import org.springframework.data.repository.CrudRepository;

public interface TypeAccidentRepository extends CrudRepository<TypeAccident, Long> {
}
