package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Item;
import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
}
