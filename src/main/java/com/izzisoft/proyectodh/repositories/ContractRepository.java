package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Contract;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContractRepository extends CrudRepository<Contract, Long> {

    List<Contract> findByEmployeeId(Long id);
}
