package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.Almacen;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AlmacenRepository extends CrudRepository<Almacen, Long> {
    Optional<List<Almacen>> findByName(String name);

    @Override
    List<Almacen> findAll();
}
