package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.AsignacionEmpleado;
import org.springframework.data.repository.CrudRepository;

public interface AsignacionEmpleadoRepository extends CrudRepository<AsignacionEmpleado, Long> {
}
