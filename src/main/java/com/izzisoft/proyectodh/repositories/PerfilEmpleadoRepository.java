package com.izzisoft.proyectodh.repositories;

import com.izzisoft.proyectodh.model.PerfilEmpleado;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PerfilEmpleadoRepository extends CrudRepository<PerfilEmpleado, Long> {

    List<PerfilEmpleado> findByEmployeeId(Long id);
}