


package com.izzisoft.proyectodh.bootsptrap;

import com.izzisoft.proyectodh.model.*;
import com.izzisoft.proyectodh.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;
    private ItemRepository itemRepository;
    private EmployeeRepository employeeRepository;
    private PositionRepository positionRepository;
    private ContractRepository contractRepository;
    private EquipoRepository equipoRepository;
    private DepartamentoRepository departamentoRepository;
    private TipoContractRepository tipoContractRepository;
    private AreaRepository areaRepository;
    private ObraRepository obraRepository;
    private AsignacionEmpleadoRepository asignacionEmpleadoRepository;
    private EtapaRepository etapaRepository;
    private PerfilEmpleadoRepository perfilEmpleadoRepository;
    private AlmacenRepository almacenRepository;
    private TipoEquipoRepository tipoEquipoRepository;


    public DevBootstrap(CategoryRepository categoryRepository, SubCategoryRepository subCategoryRepository, ItemRepository itemRepository,
                        EmployeeRepository employeeRepository, PositionRepository positionRepository, ContractRepository contractRepository,
                        DepartamentoRepository departamentoRepository, TipoContractRepository tipoContractRepository, AreaRepository areaRepository,
                        ObraRepository obraRepository, AsignacionEmpleadoRepository asignacionEmpleadoRepository, EtapaRepository etapaRepository,
                        AlmacenRepository almacenRepository, TipoEquipoRepository tipoEquipoRepository, EquipoRepository equipoRepository, PerfilEmpleadoRepository perfilEmpleadoRepository) {

        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
        this.contractRepository = contractRepository;
        this.departamentoRepository = departamentoRepository;
        this.tipoContractRepository = tipoContractRepository;
        this.areaRepository = areaRepository;
        this.obraRepository = obraRepository;
        this.asignacionEmpleadoRepository = asignacionEmpleadoRepository;
        this.etapaRepository = etapaRepository;
        this.perfilEmpleadoRepository= perfilEmpleadoRepository;
        this.almacenRepository = almacenRepository;
        this.tipoEquipoRepository = tipoEquipoRepository;
        this.equipoRepository = equipoRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent applicationEvent) {
        initData();
    }

    private void initData() {
        // EPP category
        Category eppCategory = new Category();
        eppCategory.setCode("EPP");
        eppCategory.setName("EPP");
        categoryRepository.save(eppCategory);

        // RESOURCE category
        Category resourceCategory = new Category();
        resourceCategory.setCode("RES");
        resourceCategory.setName("RESOURCE");
        categoryRepository.save(resourceCategory);

        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");
        subCategoryRepository.save(rawMaterialSubCategory);

// safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");

        subCategoryRepository.save(safetySubCategory);


        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("HELMET");
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("INK");
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);

        // add Departamento
        Departamento departamento = new Departamento();
        departamento.setDepCode("A1");
        departamento.setDepName("AVP");
        departamento.setDepTelefono(4567932);
        departamento.setDepUbicacion("Planta alta, edif. avalon");
        departamento.setDepDescripcion("Departamento de Gerencia general");
        departamentoRepository.save(departamento);

        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");
        john.setCi(132156);
        john.setTelefono(5665646);
        john.setCelular(23156);
        john.setFechaNacimiento(new Date());
        john.setDepartamento(departamento);
        john.setEstadoCivil("single");

        // Position
        Position position = new Position();
        position.setName("OPERATIVE");
        positionRepository.save(position);


        // Tipo Contrato
        TipoContract tipoContrato = new TipoContract();
        tipoContrato.setCode("AAA");
        tipoContrato.setName("BBB");
        tipoContrato.setDescripcion("lalala");
        tipoContractRepository.save(tipoContrato);

        // contract
        Contract contract = new Contract();
        contract.setEmployee(john);
        contract.setPosition(position);
        contract.setTipoContract(tipoContrato);

        john.getContracts().add(contract);
        employeeRepository.save(john);
        contractRepository.save(contract);


        //code asignacionempleado
        AsignacionEmpleado code = new AsignacionEmpleado();
        code.setCodeEmp("132156");
        code.setFechaAsignacion(new Date(2010, 1, 1));
        code.setFechaConclusion(new Date(2011, 1, 1));
        code.setFechaRetiro(new Date(2012, 1, 1));


        //segunda Etapa
        Etapa segunda = new Etapa();
        segunda.setNombre("segunda");
        segunda.setCode("1234567");
        segunda.setFechaInicio(new Date(2010, 1, 1));
        segunda.setFechaFin(new Date(2011, 8, 1));
        segunda.setNroEmpleados(12);
        etapaRepository.save(segunda);


        Area area = new Area();
        area.setResponsable(john);
        area.setCode("NVOO1");
        area.setNombre("Ariel");
        areaRepository.save(area);


        //Set<Etapa> etapas= new HashSet<>();
        //etapas.add(segunda);
        Obra obra = new Obra();
        obra.setDescripcion("nueva obra para contruccion completa");
        obra.setCode("Obra012");
        obra.setUbicacion("calle miraflores");
        obra.setNombre("contrucciones Concretec");
        obra.setFechaInicio(new Date(2010, 2, 4));
        obra.setFechaFin(new Date(2010, 9, 23));
        obraRepository.save(obra);

        PerfilEmpleado perfilEmpleado=  new PerfilEmpleado();
        perfilEmpleado.setEspecializacion("arquitecto");
        perfilEmpleado.setExperiencia(5);
        perfilEmpleado.setFormacion("academica");
        perfilEmpleado.setSector("finanzas");
        perfilEmpleadoRepository.save(perfilEmpleado);


        Almacen almacen = new Almacen();
        almacen.setResponsable(john);
        almacen.setCode("NVOO1");
        //almacen.getName();
        almacenRepository.save(almacen);

        //tipo de Equipo String code, String nombre equipo, String nombre maquina...
        TipoEquipo tipoEquipo = new TipoEquipo();
        tipoEquipo.setCode("123");
        tipoEquipo.setNombre_equipo("Maquinaria Pesada");
        tipoEquipo.setNombre_maquina("Mezcladora");
        tipoEquipoRepository.save(tipoEquipo);

        //Equipo String code, String Descripcion
        Equipo equipo = new Equipo();
        equipo.setCode("123");
        equipo.setTipoEquipo(tipoEquipo);
        equipo.setDescripcion("Equipo de Seguridad Personal");
        equipoRepository.save(equipo);
    }
}
