/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Position;

import java.util.List;

public interface PositionService extends GenericService<Position> {
}