package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.AsignacionEmpleado;
import com.izzisoft.proyectodh.repositories.AsignacionEmpleadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;


@Service
public class AsignacionEmpleadoServiceImpl extends GenericServiceImpl<AsignacionEmpleado> implements AsignacionEmpleadoService {
    private static Logger logger = LoggerFactory.getLogger(AsignacionEmpleadoServiceImpl.class);

    private AsignacionEmpleadoRepository repository;

    public AsignacionEmpleadoServiceImpl(AsignacionEmpleadoRepository repository) {
        this.repository = repository;

    }

    @Override
    protected CrudRepository<AsignacionEmpleado, Long> getRepository() {
        return repository;
    }
}
