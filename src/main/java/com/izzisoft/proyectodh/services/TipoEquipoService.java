/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.TipoEquipo;

public interface TipoEquipoService extends GenericService<TipoEquipo> {
}