package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Obra;

import java.util.Date;

public interface ObraService extends GenericService<Obra> {

    Iterable<Obra> findObraAll(String fechaInicio, String fechaFin);
}
