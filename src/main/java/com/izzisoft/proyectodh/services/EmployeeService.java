/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EmployeeCommand;
import com.izzisoft.proyectodh.model.Employee;

import java.io.InputStream;
import java.util.List;

public interface EmployeeService extends GenericService<Employee> {
    void saveImage(Long id, InputStream inputStream);
    Employee saveEmployee(EmployeeCommand employeeCommand);
    Iterable<Employee> findEmployees(Long query);
}