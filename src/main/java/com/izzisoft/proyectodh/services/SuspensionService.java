/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Suspension;

public interface SuspensionService extends GenericService<Suspension> {
}