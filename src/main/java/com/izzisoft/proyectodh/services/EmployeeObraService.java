package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EmployeeObraCommand;
import com.izzisoft.proyectodh.model.EmployeeObra;

import java.util.List;

public interface EmployeeObraService extends GenericService<EmployeeObra> {

    EmployeeObra saveEmployeeObra(EmployeeObraCommand employeeObraCommand);
    List<EmployeeObra> getEmployeeObras(Long id);
}
