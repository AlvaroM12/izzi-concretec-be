package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.ObraEtapaCommand;
import com.izzisoft.proyectodh.model.ObraEtapa;

import java.util.List;

public interface ObraEtapaService extends GenericService<ObraEtapa> {

    List<ObraEtapa> getObraEtapasByObra(Long id);
    ObraEtapa saveObraEtapa(ObraEtapaCommand obraEtapaCommand);
}
