package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Cargo;

public interface CargoService extends GenericService<Cargo> {
}
