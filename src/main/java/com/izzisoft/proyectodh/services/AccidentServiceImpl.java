package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.AccidentCommand;
import com.izzisoft.proyectodh.model.Accident;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.model.TypeAccident;
import com.izzisoft.proyectodh.repositories.AccidentRepository;
import com.izzisoft.proyectodh.repositories.EmployeeRepository;
import com.izzisoft.proyectodh.repositories.TypeAccidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccidentServiceImpl extends GenericServiceImpl<Accident> implements AccidentService {

    private AccidentRepository accidentRepository;
    private EmployeeRepository employeeRepository;
    private TypeAccidentRepository typeAccidentRepository;

    public AccidentServiceImpl(AccidentRepository accidentRepository, EmployeeRepository employeeRepository, TypeAccidentRepository typeAccidentRepository) {
        this.accidentRepository = accidentRepository;
        this.employeeRepository = employeeRepository;
        this.typeAccidentRepository = typeAccidentRepository;
    }

    @Override
    protected CrudRepository<Accident, Long> getRepository() {
        return accidentRepository;
    }

    @Override
    public Accident saveAccident(AccidentCommand accidentCommand) {
        Accident accident = accidentCommand.toEntity();
        Optional<Employee> optionalEmployee = employeeRepository.findById(accidentCommand.getEmployeeId());
        Optional<TypeAccident> optionalTypeAccident = typeAccidentRepository.findById(accidentCommand.getTypeAccidentId());
        if (optionalEmployee.isPresent())
            accident.setEmployee(optionalEmployee.get());
        if (optionalTypeAccident.isPresent())
            accident.setTypeAccident(optionalTypeAccident.get());
        accident = accidentRepository.save(accident);
        return accident;
    }

    @Override
    public Iterable<Accident> findAllAccidentes(Long tipo) {
        if (null != tipo)
            return accidentRepository.findAllByTypeAccidentId(tipo);
        return accidentRepository.findAll();
    }
}
