/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.SubCategory;
import com.izzisoft.proyectodh.repositories.SubCategoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class SubCategoryServiceImpl extends GenericServiceImpl<SubCategory> implements SubCategoryService {
    private SubCategoryRepository repository;

    public SubCategoryServiceImpl(SubCategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<SubCategory, Long> getRepository() {
        return repository;
    }
}