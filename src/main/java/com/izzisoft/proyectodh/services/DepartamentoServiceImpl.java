/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Departamento;
import com.izzisoft.proyectodh.repositories.DepartamentoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class DepartamentoServiceImpl extends GenericServiceImpl<Departamento> implements DepartamentoService {
    private static Logger logger = LoggerFactory.getLogger(DepartamentoServiceImpl.class);

    private DepartamentoRepository repository;

    public DepartamentoServiceImpl(DepartamentoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Departamento> findByName(String nombre) {
        return StringUtils.isEmpty(nombre) ? findAll() : repository.findByDepName(nombre).get();
    }

    @Override
    protected CrudRepository<Departamento, Long> getRepository() {
        return repository;
    }
}