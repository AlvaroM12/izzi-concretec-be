package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Area;


import java.util.List;

public interface AreaService extends GenericService<Area> {
    List<Area> findByNombre(String nombre);
}
