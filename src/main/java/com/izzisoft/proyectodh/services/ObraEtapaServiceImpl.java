package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.ObraEtapaCommand;
import com.izzisoft.proyectodh.model.Etapa;
import com.izzisoft.proyectodh.model.Obra;
import com.izzisoft.proyectodh.model.ObraEtapa;
import com.izzisoft.proyectodh.repositories.EtapaRepository;
import com.izzisoft.proyectodh.repositories.ObraEtapaRepository;
import com.izzisoft.proyectodh.repositories.ObraRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ObraEtapaServiceImpl extends GenericServiceImpl<ObraEtapa> implements ObraEtapaService {

    private ObraEtapaRepository obraEtapaRepository;
    private ObraRepository obraRepository;
    private EtapaRepository etapaRepository;

    public ObraEtapaServiceImpl(ObraEtapaRepository obraEtapaRepository, ObraRepository obraRepository, EtapaRepository etapaRepository) {
        this.obraEtapaRepository = obraEtapaRepository;
        this.obraRepository = obraRepository;
        this.etapaRepository = etapaRepository;
    }

    @Override
    protected CrudRepository<ObraEtapa, Long> getRepository() {
        return obraEtapaRepository;
    }

    @Override
    public List<ObraEtapa> getObraEtapasByObra(Long id) {
        return obraEtapaRepository.findByObraId(id);
    }

    @Override
    public ObraEtapa saveObraEtapa(ObraEtapaCommand obraEtapaCommand) {
        ObraEtapa obraEtapa = obraEtapaCommand.toEntity();
        Optional<Obra> optionalObra = obraRepository.findById(obraEtapaCommand.getObraId());
        Optional<Etapa> optionalEtapa = etapaRepository.findById(obraEtapaCommand.getEtapaId());
        if (optionalObra.isPresent())
            obraEtapa.setObra(optionalObra.get());
        if (optionalEtapa.isPresent())
            obraEtapa.setEtapa(optionalEtapa.get());
        obraEtapa = obraEtapaRepository.save(obraEtapa);
        return obraEtapa;
    }
}
