/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.PerfilEmpleadoCommand;
import com.izzisoft.proyectodh.model.PerfilEmpleado;

public interface PerfilEmpleadoService extends GenericService<PerfilEmpleado> {

    PerfilEmpleado findProfileByEmployeeId(Long id);
    PerfilEmpleado saveProfileEmployee(PerfilEmpleadoCommand empleadoCommand);
}