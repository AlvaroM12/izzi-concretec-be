package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.LoginComand;
import com.izzisoft.proyectodh.model.User;
import com.izzisoft.proyectodh.repositories.UserRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends GenericServiceImpl<User> implements UserService {

    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    protected CrudRepository<User, Long> getRepository() {
        return userRepository;
    }

    @Override
    public User login(LoginComand user) {
        return userRepository.findByEmailAndPassword(user.getUsername(), user.getPassword());
    }
}