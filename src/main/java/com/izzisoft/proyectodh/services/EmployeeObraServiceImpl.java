package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EmployeeObraCommand;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.model.EmployeeObra;
import com.izzisoft.proyectodh.model.Obra;
import com.izzisoft.proyectodh.repositories.EmployeeObraRepository;
import com.izzisoft.proyectodh.repositories.EmployeeRepository;
import com.izzisoft.proyectodh.repositories.ObraRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeObraServiceImpl extends GenericServiceImpl<EmployeeObra> implements EmployeeObraService {

    private EmployeeObraRepository employeeObraRepository;
    private EmployeeRepository employeeRepository;
    private ObraRepository obraRepository;

    public EmployeeObraServiceImpl(EmployeeObraRepository employeeObraRepository, EmployeeRepository employeeRepository, ObraRepository obraRepository) {
        this.employeeObraRepository = employeeObraRepository;
        this.employeeRepository = employeeRepository;
        this.obraRepository = obraRepository;
    }

    @Override
    protected CrudRepository<EmployeeObra, Long> getRepository() {
        return employeeObraRepository;
    }

    @Override
    public EmployeeObra saveEmployeeObra(EmployeeObraCommand employeeObraCommand) {
        EmployeeObra employeeObra = employeeObraCommand.toEntity();
        Optional<Employee> optionalEmployee = employeeRepository.findById(employeeObraCommand.getEmployeeId());
        Optional<Obra> optionalObra = obraRepository.findById(employeeObraCommand.getObraId());
        if (optionalEmployee.isPresent())
            employeeObra.setEmployee(optionalEmployee.get());
        if (optionalObra.isPresent())
            employeeObra.setObra(optionalObra.get());
        employeeObra = employeeObraRepository.save(employeeObra);
        return employeeObra;
    }

    @Override
    public List<EmployeeObra> getEmployeeObras(Long id) {
        return employeeObraRepository.findByEmployeeId(id);
    }
}
