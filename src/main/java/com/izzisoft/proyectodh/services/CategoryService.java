/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Category;

import java.util.List;

public interface CategoryService extends GenericService<Category> {
    List<Category> find(String code);
}
