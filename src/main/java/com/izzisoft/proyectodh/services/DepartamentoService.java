/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Departamento;

import java.util.List;

public interface DepartamentoService extends GenericService<Departamento> {
    List<Departamento> findByName(String nombre);
}