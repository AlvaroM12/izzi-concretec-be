package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EquipoCommand;
import com.izzisoft.proyectodh.model.Equipo;
import com.izzisoft.proyectodh.model.TipoEquipo;
import com.izzisoft.proyectodh.repositories.EquipoRepository;
import com.izzisoft.proyectodh.repositories.TipoEquipoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EquipoServiceImpl extends GenericServiceImpl<Equipo> implements EquipoService  {

    private EquipoRepository equipoRepository;
    private TipoEquipoRepository tipoEquipoRepository;

    public EquipoServiceImpl(EquipoRepository equipoRepository, TipoEquipoRepository tipoEquipoRepository) {
        this.equipoRepository = equipoRepository;
        this.tipoEquipoRepository = tipoEquipoRepository;
    }

    @Override
    protected CrudRepository<Equipo, Long> getRepository() {
        return equipoRepository;
    }

    @Override
    public Equipo saveEquipo(EquipoCommand equipoCommand) {
        Equipo equipo = equipoCommand.toEntity();
        Optional<TipoEquipo> optionalTipoEquipo = tipoEquipoRepository.findById(equipoCommand.getTipoEquipoId());
        if (optionalTipoEquipo.isPresent())
            equipo.setTipoEquipo(optionalTipoEquipo.get());
        equipo = equipoRepository.save(equipo);
        return equipo;
    }

    @Override
    public Iterable<Equipo> findAllEquipo(Long id) {
        if (null != id)
            return equipoRepository.findAllByTipoEquipoId(id);
        return equipoRepository.findAll();
    }
}
