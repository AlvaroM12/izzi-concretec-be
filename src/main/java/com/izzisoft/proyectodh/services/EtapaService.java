package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Etapa;

public interface EtapaService extends GenericService<Etapa> {
}
