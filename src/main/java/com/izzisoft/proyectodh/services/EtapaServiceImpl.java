package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Etapa;
import com.izzisoft.proyectodh.repositories.EtapaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class EtapaServiceImpl extends GenericServiceImpl<Etapa> implements EtapaService {
    private static Logger logger = LoggerFactory.getLogger(EtapaServiceImpl.class);
    private EtapaRepository repository;

    public EtapaServiceImpl(EtapaRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Etapa, Long> getRepository() {
        return repository;
    }
}
