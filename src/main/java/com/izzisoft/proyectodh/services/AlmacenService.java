/**
 * @author:
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Almacen;
import com.izzisoft.proyectodh.model.Area;

import java.util.List;

public interface AlmacenService extends GenericService<Almacen> {
    List<Almacen> findByName(String name);
}