
package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.AsignacionEquipo;
import com.izzisoft.proyectodh.repositories.AsignacionEquipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class AsignacionEquipoServiceImpl extends GenericServiceImpl<AsignacionEquipo> implements AsignacionEquipoService {
    private static Logger logger = LoggerFactory.getLogger(AsignacionEquipoServiceImpl.class);

    private AsignacionEquipoRepository repository;

    public AsignacionEquipoServiceImpl(AsignacionEquipoRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<AsignacionEquipo, Long> getRepository() {
        return repository;
    }
}