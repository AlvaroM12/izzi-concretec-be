package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.ContractCommand;
import com.izzisoft.proyectodh.model.Contract;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.model.TipoContract;
import com.izzisoft.proyectodh.repositories.ContractRepository;
import com.izzisoft.proyectodh.repositories.EmployeeRepository;
import com.izzisoft.proyectodh.repositories.TipoContractRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl extends GenericServiceImpl<Contract> implements ContractService {

    private ContractRepository contractRepository;
    private EmployeeRepository employeeRepository;
    private TipoContractRepository tipoContractRepository;

    public ContractServiceImpl(ContractRepository contractRepository, EmployeeRepository employeeRepository, TipoContractRepository tipoContractRepository) {
        this.contractRepository = contractRepository;
        this.employeeRepository = employeeRepository;
        this.tipoContractRepository = tipoContractRepository;
    }

    @Override
    protected CrudRepository<Contract, Long> getRepository() {
        return contractRepository;
    }

    @Override
    public Contract saveContractEmployee(ContractCommand contractCommand) {
        Contract contract = contractCommand.toContract();
        Optional<Employee> optionalEmployee = employeeRepository.findById(contractCommand.getEmployeeId());
        Optional<TipoContract> optionalTipoContract = tipoContractRepository.findById(contractCommand.getTipoContractId());
        if (optionalEmployee.isPresent())
            contract.setEmployee(optionalEmployee.get());
        if (optionalTipoContract.isPresent())
            contract.setTipoContract(optionalTipoContract.get());
        contract = contractRepository.save(contract);
        return contract;
    }

    @Override
    public Contract getContractByEmployee(Long id) {
        List<Contract> contracts = contractRepository.findByEmployeeId(id);
        if (!contracts.isEmpty()) {
            return contracts.get(0);
        }
        return new Contract();
    }
}
