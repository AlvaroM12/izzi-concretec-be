package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.LoginComand;
import com.izzisoft.proyectodh.model.User;

public interface UserService extends GenericService<User> {
    public User login(LoginComand user);
}