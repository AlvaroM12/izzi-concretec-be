package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.AccidentCommand;
import com.izzisoft.proyectodh.model.Accident;
import org.hibernate.validator.constraints.SafeHtml;

import java.util.List;

public interface AccidentService extends GenericService<Accident> {

    Accident saveAccident(AccidentCommand accidentCommand);
    Iterable<Accident> findAllAccidentes(Long tipo);
}
