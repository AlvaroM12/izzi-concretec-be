package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.TipoContract;
import com.izzisoft.proyectodh.repositories.TipoContractRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContractServiceImpl extends GenericServiceImpl<TipoContract> implements TipoContractService{

    private TipoContractRepository tipoContractRepository;

    public TipoContractServiceImpl(TipoContractRepository tipoContractRepository) {
        this.tipoContractRepository = tipoContractRepository;
    }

    @Override
    protected CrudRepository<TipoContract, Long> getRepository() {
        return tipoContractRepository;
    }
}
