/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Item;

import java.io.InputStream;

public interface ItemService extends GenericService<Item> {
    void saveImage(Long id, InputStream file);
}