package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Cargo;
import com.izzisoft.proyectodh.repositories.CargoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class CargoServiceImpl extends GenericServiceImpl<Cargo> implements CargoService {

    private CargoRepository cargoRepository;

    public CargoServiceImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    @Override
    protected CrudRepository<Cargo, Long> getRepository() {
        return cargoRepository;
    }
}
