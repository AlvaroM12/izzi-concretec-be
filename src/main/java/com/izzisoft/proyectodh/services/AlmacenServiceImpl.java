/**
 * @author: edson
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Almacen;
import com.izzisoft.proyectodh.repositories.AlmacenRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class AlmacenServiceImpl extends GenericServiceImpl<Almacen> implements AlmacenService {
    private AlmacenRepository repository;

    public AlmacenServiceImpl(AlmacenRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Almacen> findByName(String name) {
        return StringUtils.isEmpty(name) ? findAll() : repository.findByName(name).get();
    }

    @Override
    protected CrudRepository<Almacen, Long> getRepository() {
        return repository;
    }
}