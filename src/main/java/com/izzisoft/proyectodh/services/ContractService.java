package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.ContractCommand;
import com.izzisoft.proyectodh.model.Contract;

public interface ContractService extends GenericService<Contract>{

    Contract saveContractEmployee(ContractCommand contractCommand);
    Contract getContractByEmployee(Long id);
}
