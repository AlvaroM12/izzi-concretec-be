/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {
}