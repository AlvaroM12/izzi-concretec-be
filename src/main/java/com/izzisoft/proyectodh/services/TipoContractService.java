package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.TipoContract;

public interface TipoContractService extends GenericService<TipoContract> {
}
