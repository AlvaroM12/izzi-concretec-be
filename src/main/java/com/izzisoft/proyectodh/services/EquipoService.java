package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EquipoCommand;
import com.izzisoft.proyectodh.model.Equipo;

public interface EquipoService extends GenericService<Equipo> {

    Equipo saveEquipo(EquipoCommand equipoCommand);
    Iterable<Equipo> findAllEquipo(Long id);
}
