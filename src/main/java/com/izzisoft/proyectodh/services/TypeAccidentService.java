package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.TypeAccident;

public interface TypeAccidentService extends GenericService<TypeAccident> {
}
