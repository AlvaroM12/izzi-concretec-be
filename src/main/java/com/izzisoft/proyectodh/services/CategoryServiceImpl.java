/**
 * @author: edson
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.Category;
import com.izzisoft.proyectodh.repositories.CategoryRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category> implements CategoryService {
    private CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Category> find(String code) {
        return StringUtils.isEmpty(code) ? findAll() : repository.findByCode(code).get();
    }

    @Override
    protected CrudRepository<Category, Long> getRepository() {
        return repository;
    }
}