package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EmployeeEquipoCommand;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.model.EmployeeEquipo;
import com.izzisoft.proyectodh.model.Equipo;
import com.izzisoft.proyectodh.repositories.EmployeeEquipoRepository;
import com.izzisoft.proyectodh.repositories.EmployeeRepository;
import com.izzisoft.proyectodh.repositories.EquipoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeEquipoServiceImpl extends GenericServiceImpl<EmployeeEquipo> implements EmployeeEquipoService {

    private EmployeeEquipoRepository employeeEquipoRepository;
    private EmployeeRepository employeeRepository;
    private EquipoRepository equipoRepository;

    public EmployeeEquipoServiceImpl(EmployeeEquipoRepository employeeEquipoRepository, EmployeeRepository employeeRepository, EquipoRepository equipoRepository) {
        this.employeeEquipoRepository = employeeEquipoRepository;
        this.employeeRepository = employeeRepository;
        this.equipoRepository = equipoRepository;
    }

    @Override
    public List<EmployeeEquipo> findEmployeeEquipoByEmployee(Long id) {
        return employeeEquipoRepository.findByEmployeeId(id);
    }

    @Override
    public EmployeeEquipo saveEmployeeEquipo(EmployeeEquipoCommand employeeEquipoCommand) {
        EmployeeEquipo employeeEquipo = employeeEquipoCommand.toEntity();
        Optional<Employee> optionalEmployee = employeeRepository.findById(employeeEquipoCommand.getEmployeeId());
        Optional<Equipo> optionalEquipo = equipoRepository.findById(employeeEquipoCommand.getEquipoId());
        if (optionalEmployee.isPresent())
            employeeEquipo.setEmployee(optionalEmployee.get());
        if (optionalEquipo.isPresent())
            employeeEquipo.setEquipo(optionalEquipo.get());
        employeeEquipo = employeeEquipoRepository.save(employeeEquipo);
        return employeeEquipo;
    }

    @Override
    protected CrudRepository<EmployeeEquipo, Long> getRepository() {
        return employeeEquipoRepository;
    }
}
