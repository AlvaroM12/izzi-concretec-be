/**
 * @author: Edson A. Terceros T.
 */

package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EmployeeCommand;
import com.izzisoft.proyectodh.model.Cargo;
import com.izzisoft.proyectodh.model.Departamento;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.repositories.CargoRepository;
import com.izzisoft.proyectodh.repositories.DepartamentoRepository;
import com.izzisoft.proyectodh.repositories.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl extends GenericServiceImpl<Employee> implements EmployeeService {
    private static Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);
    private EmployeeRepository employeeRepository;
    private DepartamentoRepository departamentoRepository;
    private CargoRepository cargoRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository, DepartamentoRepository departamentoRepository, CargoRepository cargoRepository) {
        this.employeeRepository = employeeRepository;
        this.departamentoRepository = departamentoRepository;
        this.cargoRepository = cargoRepository;
    }

    @Override
    protected CrudRepository<Employee, Long> getRepository() {
        return employeeRepository;
    }

    @Override
    public void saveImage(Long id, InputStream file) {
        Employee employeePersisted = findById(id);
        try {
            Byte[] bytes = ImageUtils.inputStreamToByteArray(file);
            employeePersisted.setImage(bytes);
            getRepository().save(employeePersisted);
        } catch (IOException e) {
            logger.error("Error reading file", e);
            e.printStackTrace();
        }
    }

    @Override
    public Employee saveEmployee(EmployeeCommand employeeCommand) {
        Optional<Departamento> departamento = departamentoRepository.findById(employeeCommand.getDepartamentId());
        Optional<Cargo> optionalCargo = cargoRepository.findById(employeeCommand.getCargoId());
        Employee employee = employeeCommand.toEmployee();
        if (departamento.isPresent()) {
            employee.setDepartamento(departamento.get());
            employee.setCargo(optionalCargo.get());
            employee = employeeRepository.save(employee);
        }
        return employee;
    }

    @Override
    public Iterable<Employee> findEmployees(Long query) {
        if (null != query){
            return employeeRepository.findByDepartamentoId(query);
        }
        return employeeRepository.findAll();
    }
}