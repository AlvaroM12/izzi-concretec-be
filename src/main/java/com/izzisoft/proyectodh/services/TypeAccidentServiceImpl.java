package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.TypeAccident;
import com.izzisoft.proyectodh.repositories.TypeAccidentRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class TypeAccidentServiceImpl extends GenericServiceImpl<TypeAccident> implements TypeAccidentService {

    private TypeAccidentRepository typeAccidentRepository;

    public TypeAccidentServiceImpl(TypeAccidentRepository typeAccidentRepository) {
        this.typeAccidentRepository = typeAccidentRepository;
    }

    @Override
    protected CrudRepository<TypeAccident, Long> getRepository() {
        return typeAccidentRepository;
    }
}
