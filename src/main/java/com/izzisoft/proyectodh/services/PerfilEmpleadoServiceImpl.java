package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.PerfilEmpleadoCommand;
import com.izzisoft.proyectodh.model.Employee;
import com.izzisoft.proyectodh.model.PerfilEmpleado;
import com.izzisoft.proyectodh.repositories.EmployeeRepository;
import com.izzisoft.proyectodh.repositories.PerfilEmpleadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PerfilEmpleadoServiceImpl extends GenericServiceImpl<PerfilEmpleado> implements PerfilEmpleadoService {
    private static Logger logger = LoggerFactory.getLogger(PerfilEmpleadoServiceImpl.class);

    private PerfilEmpleadoRepository perfilEmpleadoRepository;
    private EmployeeRepository employeeRepository;

    public PerfilEmpleadoServiceImpl(PerfilEmpleadoRepository perfilEmpleadoRepository, EmployeeRepository employeeRepository) {
        this.perfilEmpleadoRepository = perfilEmpleadoRepository;
        this.employeeRepository = employeeRepository;
    }

    @Override
    protected CrudRepository<PerfilEmpleado, Long> getRepository() {
        return perfilEmpleadoRepository;
    }

    @Override
    public PerfilEmpleado findProfileByEmployeeId(Long id) {
        List<PerfilEmpleado> perfilEmpleados = perfilEmpleadoRepository.findByEmployeeId(id);
        if (!perfilEmpleados.isEmpty())
            return perfilEmpleados.get(0);
        return new PerfilEmpleado();
    }

    @Override
    public PerfilEmpleado saveProfileEmployee(PerfilEmpleadoCommand empleadoCommand) {
        PerfilEmpleado perfilEmpleado = empleadoCommand.toPerfilEmpleado();
        Optional<Employee> optionalEmployee = employeeRepository.findById(empleadoCommand.getEmployeeId());
        if (optionalEmployee.isPresent()) {
            perfilEmpleado.setEmployee(optionalEmployee.get());
            perfilEmpleado = perfilEmpleadoRepository.save(perfilEmpleado);
        }
        return perfilEmpleado;
    }
}