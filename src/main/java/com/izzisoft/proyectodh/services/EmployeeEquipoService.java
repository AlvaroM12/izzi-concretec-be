package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.command.EmployeeEquipoCommand;
import com.izzisoft.proyectodh.model.EmployeeEquipo;

import java.util.List;

public interface EmployeeEquipoService extends GenericService<EmployeeEquipo> {

    List<EmployeeEquipo> findEmployeeEquipoByEmployee(Long id);
    EmployeeEquipo saveEmployeeEquipo(EmployeeEquipoCommand employeeEquipoCommand);
}
