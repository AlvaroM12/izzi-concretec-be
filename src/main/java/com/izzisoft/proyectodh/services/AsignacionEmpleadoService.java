package com.izzisoft.proyectodh.services;

import com.izzisoft.proyectodh.model.AsignacionEmpleado;

public interface AsignacionEmpleadoService extends GenericService<AsignacionEmpleado> {
}
