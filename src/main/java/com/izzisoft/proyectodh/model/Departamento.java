package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;

@Entity
public class Departamento extends ModelBase {
    private String depName;
    private String depCode;
    private String depUbicacion;
    private int depTelefono;
    private String depDescripcion;

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getDepCode() {
        return depCode;
    }

    public void setDepCode(String depCode) {
        this.depCode = depCode;
    }

    public String getDepUbicacion() {
        return depUbicacion;
    }

    public void setDepUbicacion(String depUbicacion) {
        this.depUbicacion = depUbicacion;
    }

    public int getDepTelefono() {
        return depTelefono;
    }

    public void setDepTelefono(int depTelefono) {
        this.depTelefono = depTelefono;
    }

    public String getDepDescripcion() {
        return depDescripcion;
    }

    public void setDepDescripcion(String depDescripcion) {
        this.depDescripcion = depDescripcion;
    }
}
