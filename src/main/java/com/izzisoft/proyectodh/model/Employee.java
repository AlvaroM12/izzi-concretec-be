
package com.izzisoft.proyectodh.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Employee extends ModelBase {
    private String firstName;
    private String lastName;
    private int ci;
    private int telefono;
    private int celular;
    private Date fechaNacimiento;
    private String estadoCivil;
    private String direccion;
    private String segundaDireccion;

    @ManyToOne
    @JoinColumn(name = "departament_id")
    private Departamento departamento;
    @ManyToOne
    @JoinColumn(name = "cargo_id")
    private Cargo cargo;

    @Lob
    private Byte[] image;

    @OneToMany(mappedBy = "employee", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JsonIgnore
    private Set<Contract> contracts = new HashSet<>();


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public int getTelefono()
    {
        return telefono;
    }

    public void setTelefono(int telefono)
    {
        this.telefono = telefono;
    }

    public int getCelular()
    {
        return celular;
    }

    public void setCelular(int celular)
    {
        this.celular = celular;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci)
    {
        this.ci = ci;
    }

    public Date getFechaNacimiento()
    {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento)
    {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEstadoCivil()
    {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil)
    {
        this.estadoCivil = estadoCivil;
    }

    public String getDireccion()
    {
        return direccion;
    }

    public void setDireccion(String direccion)
    {
        this.direccion = direccion;
    }

    public String getSegundaDireccion() {
        return segundaDireccion;
    }

    public void setSegundaDireccion(String segundaDireccion)
    {
        this.segundaDireccion = segundaDireccion;
    }

    public Set<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(Set<Contract> contracts)
    {
        this.contracts = contracts;
    }


    public Byte[] getImage()
    {
        return image;
    }

    public void setImage(Byte[] image)
    {
        this.image = image;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
}
