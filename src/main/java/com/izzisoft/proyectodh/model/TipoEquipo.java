
package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;

@Entity
public class TipoEquipo extends ModelBase {

    private String code;
    private String nombre_equipo;
    private String nombre_maquina;

    public String getCode() {
        return code;
    }

    public String getNombre_equipo() {
        return nombre_equipo;
    }

    public String getNombre_maquina() {
        return nombre_maquina;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setNombre_equipo(String nombre_equipo) {
        this.nombre_equipo = nombre_equipo;
    }

    public void setNombre_maquina(String nombre_maquina) {
        this.nombre_maquina = nombre_maquina;
    }
    
    
}
