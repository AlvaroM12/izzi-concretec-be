package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class EmployeeObra extends ModelBase {

    @ManyToOne(optional = false)
    private Employee employee;

    @ManyToOne(optional = false)
    private Obra obra;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Obra getObra() {
        return obra;
    }

    public void setObra(Obra obra) {
        this.obra = obra;
    }
}
