
package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class PerfilEmpleado extends ModelBase {

    private int experiencia;
    private String formacion;
    private String especializacion;
    private String sector;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    public int getExperiencia() {
        return experiencia;
    }

    public String getFormacion() {
        return formacion;
    }

    public String getEspecializacion() {
        return especializacion;
    }

    public String getSector() {
        return sector;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public void setFormacion(String formacion) {
        this.formacion = formacion;
    }

    public void setEspecializacion(String especializacion)
    {
        this.especializacion = especializacion;
    }

    public void setSector(String sector)
    {
        this.sector = sector;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}