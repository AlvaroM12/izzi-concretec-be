
package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;

@Entity
public class TipoContract extends ModelBase {

    private String code;
    private String name;
    private String descripcion;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
