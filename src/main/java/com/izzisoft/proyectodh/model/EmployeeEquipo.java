package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class EmployeeEquipo extends ModelBase {

    @ManyToOne
    private Employee employee;

    @ManyToOne
    private Equipo equipo;

    private String description;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
