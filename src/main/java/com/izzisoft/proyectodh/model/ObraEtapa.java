package com.izzisoft.proyectodh.model;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class ObraEtapa extends ModelBase{

    @ManyToOne
    private Etapa etapa;

    @ManyToOne
    private Obra obra;

    private String description;

    private String status;

    public Etapa getEtapa() {
        return etapa;
    }

    public void setEtapa(Etapa etapa) {
        this.etapa = etapa;
    }

    public Obra getObra() {
        return obra;
    }

    public void setObra(Obra obra) {
        this.obra = obra;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
