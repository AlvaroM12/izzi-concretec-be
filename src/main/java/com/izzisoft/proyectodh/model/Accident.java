package com.izzisoft.proyectodh.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Accident extends ModelBase {

    @ManyToOne
    private Employee employee;

    @ManyToOne
    private TypeAccident typeAccident;

    private String description;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public TypeAccident getTypeAccident() {
        return typeAccident;
    }

    public void setTypeAccident(TypeAccident typeAccident) {
        this.typeAccident = typeAccident;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
